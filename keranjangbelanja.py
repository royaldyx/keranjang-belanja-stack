class KeranjangBelanja:
    def __init__(self):
        self.tumpukan = []

    def tambahkan_barang(self, barang):
        self.tumpukan.append(barang)
        print(f"Barang {barang} ditambahkan ke dalam keranjang belanja.")

    def hapus_barang(self):
        if not self.is_empty():
            print("Pilihan:")
            print("1. Hapus barang terakhir")
            print("2. Hapus semua barang")
            pilihan_hapus = input("Pilih opsi: ")

            if pilihan_hapus == "1":
                barang = self.tumpukan.pop()
                print(f"Barang {barang} dihapus dari keranjang belanja.")
            elif pilihan_hapus == "2":
                self.tumpukan.clear()
                print("Semua barang dihapus dari keranjang belanja.")
            else:
                print("Pilihan tidak valid.")
        else:
            print("Keranjang belanja kosong. Tidak ada barang untuk dihapus.")

    def cek_isi_keranjang(self):
        if not self.is_empty():
            print("Isi keranjang belanja:")
            for barang in reversed(self.tumpukan):
                print("-", barang)

            print("Pilihan:")
            print("1. Lihat jumlah barang yang ada di keranjang")
            print("2. Hapus barang dari keranjang")
            print("3. Kembali")
            pilihan = input("Pilih menu: ")

            if pilihan == "1":
                self.lihat_jumlah_barang()
            elif pilihan == "2":
                self.hapus_barang()
            elif pilihan == "3":
                pass  # Kembali ke menu utama
            else:
                print("Pilihan tidak valid.")
        else:
            print("Keranjang belanja kosong.")

    def lihat_jumlah_barang(self):
        jumlah_barang = len(self.tumpukan)
        print(f"Jumlah barang di keranjang: {jumlah_barang}")

    def is_empty(self):
        return len(self.tumpukan) == 0

    def keluarkan_barang_ke_kasir(self):
        if not self.is_empty():
            while not self.is_empty():
                barang = self.tumpukan.pop()
                print(f"Mengeluarkan barang {barang} ke kasir.")
        else:
            print("Keranjang belanja kosong. Tidak ada barang untuk dikeluarkan.")


# Contoh penggunaan

keranjang = KeranjangBelanja()

while True:
    print("Menu:")
    print("1. Tambahkan barang ke keranjang")
    print("2. Cek isi keranjang")
    print("3. Keluarkan barang ke kasir")
    print("4. Keluar")

    pilihan = input("Pilih menu: ")

    if pilihan == "1":
        barang = input("Masukkan nama barang: ")
        keranjang.tambahkan_barang(barang)
    elif pilihan == "2":
        keranjang.cek_isi_keranjang()
    elif pilihan == "3":
        keranjang.keluarkan_barang_ke_kasir()
    elif pilihan == "4":
        print("Terima kasih. Sampai jumpa!")
        break
    else:
        print("Pilihan tidak valid. Silakan pilih menu yang tersedia.")

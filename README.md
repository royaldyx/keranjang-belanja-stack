## Berikut adalah cara untuk mencoba program ini di tempat Anda:

- Buka terminal atau command prompt di komputer Anda.
- Ketik perintah git clone https://gitlab.com/royaldyx/keranjang-belanja-stack.git untuk mengkloning repositori ke 
  dalam direktori lokal Anda. Jika Anda tidak memiliki Git terinstal, Anda juga dapat mengunduh repositori ini dengan mengeklik tombol "Download" di halaman repositori tersebut.
- Jika Anda memilih untuk menyalin script programnya, buka file dengan ekstensi .py menggunakan Visual Studio Code 
  atau editor kode lainnya yang sudah terinstal Python.
- Pastikan Anda telah menginstal Python di komputer Anda. Jika belum, Anda dapat mengunduhnya dari situs resmi  
  Python dan mengikuti panduan instalasinya.
- Setelah menginstal Python, buka terminal atau command prompt di direktori tempat Anda menyimpan file program.
- Ketik perintah python nama_file.py, dengan menggantikan nama_file.py dengan nama file Python yang Anda gunakan.
- Program akan mulai berjalan, dan Anda dapat mengikuti instruksinya untuk menggunakan keranjang belanja stack.
- Selamat mencoba!

Pastikan Anda telah menginstal Git dan Python dengan benar sebelum mengikuti langkah-langkah di atas. Jika Anda mengalami kesulitan, pastikan untuk merujuk ke dokumentasi resmi atau mencari bantuan online terkait instalasi dan penggunaan Git serta Python.
